// Create a program with multiple string variables and that holds your name in a string.
package main

import "fmt"

/*func holdString(a,b string) {
	a,b := "mehul", "mahesh"
	return a,b
}*/
var (
	fName string
	lName string
)

func holdString(name ...string) string  {
	writeName := ""
	for _, naam := range name{
		writeName += naam
	}
	return writeName
}

func main()  {
	var a,b string
	a = "Mehul"
	b = "Mahesh"
	fmt.Println(a)
	fmt.Println(b)

	fName = "Suresh"
	lName = "Panchal"
	fmt.Println(fName,lName)

	name := holdString("mehul"," , ","rahul")
	fmt.Println(name)
}
