// Make a program that divides x by 2 if it’s greater than 0
package main

import "fmt"

var divi int

func xDivide()  {
	fmt.Println("insert any to divide it by 2 :")
	fmt.Scanf("%d", &divi)

	if divi > 0 {
		fmt.Println(divi/2)
	} else {
		fmt.Println("something went wrong")
	}
}

func main()  {
	xDivide()
}