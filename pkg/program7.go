// Make a program that counts from 1 to 10.
package main

import (
	"fmt"
)

var (
	numb int
)

func numCount() {
	fmt.Println("insert a number which you wanted to print counts from 1 : ")
	fmt.Scanf("%d", &numb)

	for x := 1; x <= numb ; x ++{
		fmt.Println(x)
	}
}

func main()  {
	numCount()
}