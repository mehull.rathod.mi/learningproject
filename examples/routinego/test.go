package main

import (
	"fmt"
	"time"
)

func doSomething(str string) {
	//time.Sleep(2 * time.Second)
	for i := 1; i <= 3; i++ {
		// make a new channel
		i := make(chan string)
		go func() {
			// write to channel
			i <- "Hello World!"
		}()

		// read from channel
		message :=  <-i
		fmt.Printf("%s: %s \n", str, message)
		time.Sleep(5 * time.Second)
	}
}

func main() {
	// calling this function the normal way
	doSomething("Hello")

	// Running it inside a go routine
	go doSomething("World")

	go func() {
		fmt.Print("Go routines are awesome")
	}()
}