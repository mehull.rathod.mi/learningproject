// Create a Program that shows your name and address
package main

import "fmt"

type Person struct {
	Name string
	Address string
}

func personName(Name, Address string) *Person{
	p := Person{
		Name: Name,
		Address: Address,
	}
	return &p
}

func main() {
	result := Person{Name: "Mehul", Address: "1184, naroda"}
	fmt.Println(result)
}
